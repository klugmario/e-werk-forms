FROM php

ENV MAIL_RCPT ""
ENV MAIL_HOST ""
ENV MAIL_PORT ""
ENV MAIL_SECURE ""
ENV MAIL_USERNAME ""
ENV MAIL_PASSWORD ""

RUN apt-get update \
    && apt-get install -y wkhtmltopdf \
    && rm -Rf /var/cache/apt/archives

ADD . /app

WORKDIR /app

CMD ["php", "-S", "0.0.0.0:1111", "plugin.php"]