<?php
/** @var $attr array */

if ( ! isset($attr['selectedForm']) || ! $attr['selectedForm']) {
    echo '';
} else {
    $domId = md5(microtime());
    ?>
    <div class="sf-form" data-form-id="<?= $attr['selectedForm'] ?>" id="form-<?= $domId ?>"></div>

    <script>
        renderSFForm('#form-<?= $domId ?>', '<?= dirname(plugin_dir_url(__FILE__)) ?>/plugin.php')
    </script>
<?php
}
