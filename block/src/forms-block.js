import {registerBlockType, serialize} from '@wordpress/blocks';
import { useBlockProps, InspectorControls } from '@wordpress/block-editor';

import { SelectControl, TextControl, PanelBody } from '@wordpress/components'

const { ServerSideRender } = wp.editor

const formIds = []

/*
 * Unfortunately wordpress does not offer a callback when ServerSideRender is loaded.
 * This interval watches the dom if new sliders to register appear
 */
setInterval(() => {
    Array.from(document.querySelectorAll('.sf-form'))
        .filter(form => ! formIds.includes(form.id))
        .forEach(form => {
            if ( ! form.id) {
                return
            }
            renderSFForm(`#${form.id}`, cs_data.backendUrl)
            formIds.push(form.id)
        })
}, 500)

const icon = wp.element.createElement('svg',
    {
        width: 20,
        height: 20,
        viewBox: '0 0 448 512'
    },
    wp.element.createElement('path', {
        d: 'M448 75.2v361.7c0 24.3-19 43.2-43.2 43.2H43.2C19.3 480 0 461.4 0 436.8V75.2C0 51.1 18.8 32 43.2 32h361.7c24 0 43.1 18.8 43.1 43.2zm-37.3 361.6V75.2c0-3-2.6-5.8-5.8-5.8h-9.3L285.3 144 224 94.1 162.8 144 52.5 69.3h-9.3c-3.2 0-5.8 2.8-5.8 5.8v361.7c0 3 2.6 5.8 5.8 5.8h361.7c3.2.1 5.8-2.7 5.8-5.8zM150.2 186v37H76.7v-37h73.5zm0 74.4v37.3H76.7v-37.3h73.5zm11.1-147.3l54-43.7H96.8l64.5 43.7zm210 72.9v37h-196v-37h196zm0 74.4v37.3h-196v-37.3h196zm-84.6-147.3l64.5-43.7H232.8l53.9 43.7zM371.3 335v37.3h-99.4V335h99.4z'
    })
)

let forms = []

fetch(cs_data.backendUrl + '?get-forms')
    .then(res => res.json())
    .then(res => {
        forms = res
    })

registerBlockType( 'sf/forms-block', {
    apiVersion: 2,
    title: 'Formulare',
    icon,
    category: 'moerth_moerth',
    attributes: {
        selectedForm: {
            type: 'string'
        }
    },
    example: {},

    edit({ name, attributes, setAttributes, isSelected }) {
        const blockProps = useBlockProps();

        const availableForms = [
            { value: 0, label: '-' },
            ...forms.map(form => ({ value: form.id, label: form.name }))
        ]

        return (
            <div { ...blockProps}>
                <InspectorControls>
                    <PanelBody>
                        <SelectControl
                            label="Formular"
                            value={attributes.selectedForm}
                            options={availableForms}
                            onChange={id => setAttributes({ selectedForm: id })}/>
                    </PanelBody>
                </InspectorControls>

                {attributes.selectedForm > 0
                    && <ServerSideRender block={name} attributes={{ selectedForm: attributes.selectedForm }} />
                    || <i>Bitte wählen Sie das gewünschte Formular in der Seitenleiste</i>
                }
            </div>
        );
    },
    save: () => { return null }
} );
