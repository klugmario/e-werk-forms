<?php
add_action( 'init', function() {
    wp_register_script('sf-forms-block', plugin_dir_url(__FILE__) . '/build/index.js');
    wp_enqueue_style('sf-forms-block', plugin_dir_url(__FILE__) . '/build/index.css');

    wp_localize_script(
        'sf-forms-block',
        'cs_data',
        [
            'backendUrl' => dirname(plugin_dir_url(__FILE__)) . '/plugin.php'
        ]
    );

    register_block_type('sf/forms-block', [
        'api_version' => 2,
        'editor_script' => 'sf-forms-block',
        'editor_style' => 'sf-forms-block',
        'attributes' => [
            'selectedForm' => [ 'type' => 'string' ]
        ],
        'render_callback' => function ($attr) {
            ob_start();
            require __DIR__ . DIRECTORY_SEPARATOR . 'forms-block-callback.php';
            return ob_get_clean();
        }
    ]);

    add_filter( 'block_categories_all', function ($categories) {
        $category_slugs = wp_list_pluck($categories, 'slug');

        return in_array('moerth_moerth', $category_slugs) ? $categories : array_merge(
            [
                [
                    'slug' => 'moerth_moerth',
                    'title' => __('Mörth & Mörth'),
                    'icon' => 'data:image/svg+xml;base64,' . base64_encode( '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 5.292 5.292"><path d="M1.135.606v.35H4.15v-.35zm1.48 1.044a1.507 1.507 0 10.061 3.013 1.507 1.507 0 00-.06-3.013zm.03.361a1.145 1.145 0 11.001 2.291 1.145 1.145 0 010-2.29z" fill="black"/></svg>')
                ]
            ],
            $categories
        );
    });
});
