import Form, {IPageConfigData} from "./src/elements/Form";

import './scss/sf-form.scss'

window['renderSFForm'] = (selector: string, backendUrl: string) => {
    const elem = document.querySelector(selector)

    if ( ! elem) {
        throw new Error(`form div does not exist: ${selector}`)
    }

    let formId = 0
    if (elem.hasAttribute('data-form-id')) {
        formId = parseInt(elem.getAttribute('data-form-id'))
    }

    if ( ! formId) {
        throw new Error('form div does not have required "data-form-id" attribute')
    }

    fetch(`${backendUrl}?form-id=${formId}`)
        .then(res => res.json())
        .then(config => {
            const form = Form.fromJson(config, backendUrl)
            form.render(selector)
        })
}
