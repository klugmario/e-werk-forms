const storedData = sessionStorage.getItem('sf-form-data')
const data = storedData ? JSON.parse(storedData) : {
    fields: {},
    data: {}
}

const handler = {
    get(target, key) {
        if (typeof target[key] === 'object' && target[key] !== null) {
            return new Proxy(target[key], handler)
        } else {
            return target[key];
        }
    },

    set: (obj, prop, value) => {
        obj[prop] = value

        setTimeout(() => {
            sessionStorage.setItem('sf-form-data', JSON.stringify(data))
        }, 100)

        return true
    }
}

// @ts-ignore
const store = new Proxy(data, handler)

export default store
