import PageList from "./PageList";
import Page, {IPage} from "./Page";
import store from "../store";

export interface IPageConfigData {
    name: string;
    pages: IPage[]
}

export default class Form {
    private name: string;
    private backendUrl: string;

    private pages: PageList = new PageList()

    private container?: HTMLDivElement

    private btnPrev?: HTMLAnchorElement
    private btnNext?: HTMLAnchorElement
    private btnSend: HTMLAnchorElement;

    private hasMandatoryFields: boolean = false

    private noticeMandatory?: HTMLDivElement
    private currentPage: number = 1
    private isValid: boolean | null;

    public static fromJson(data: IPageConfigData, backendUrl: string): Form {
        if ('undefined' === typeof data.name) {
            throw new Error('json is missing "name" key')
        }

        if ('undefined' === typeof data.pages) {
            throw new Error('json is missing "pages" key')
        }

        const form = new Form(data.name, backendUrl)

        for (const pageData of data.pages) {
            form.addPage(
                Page.fromJson(pageData)
            )
        }

        return form
    }

    constructor(name: string, backendUrl: string) {
        this.name = name
        this.backendUrl = backendUrl

        const pageNum = sessionStorage.getItem('pagenum')

        if (pageNum) {
            this.currentPage = parseInt(pageNum)
        }
    }

    private addPage(page: Page) {
        this.pages.add(page)
    }

    render(selector: string) {
        const target = document.querySelector(selector)

        if ('undefined' === typeof target) {
            throw new Error('no element with selector found: ' + selector)
        }

        if ( ! this.container) {
            this.container = document.createElement('div')
            this.container.classList.add('sf-form')
            target.appendChild(this.container)
        }

        this.update()
    }

    private update() {
        this.updateHeader()
        this.updatePage()
        this.updateFooter()
    }

    private renderHeader() {
        let container = this.container.querySelector('header')
        let list = null

        if ( ! container) {
            container = document.createElement('header')
            list = document.createElement('ul')
            container.appendChild(list)
            this.container.appendChild(container)
        } else {
            list = container.querySelector('ul')
        }

        const items = []

        for (let page of this.pages.all()) {
            const item = document.createElement('li')
            item.setAttribute('data-page', '' + page.sort)

            const sort = document.createElement('div')
            sort.classList.add('sort')
            sort.innerText = '' + page.sort

            const name = document.createElement('span')
            name.innerText = page.name

            item.appendChild(sort)
            item.appendChild(name)

            items.push(item)
        }

        list.innerHTML = ''
        for (let item of items) {
            list.appendChild(item)
        }

        return container
    }

    private updatePage() {
        let container = this.container.querySelector('.page')

        if ( ! container) {
            container = document.createElement('div')
            container.classList.add('page')

            const headline = document.createElement('h1')

            const pagename = document.createElement('span')
            pagename.classList.add('pagename')
            pagename.innerText = this.pages.get(this.currentPage).name

            const pagenum = document.createElement('span')
            pagenum.classList.add('pagenum')
            pagenum.innerHTML = ` (${this.currentPage}/${this.pages.count()})`

            headline.appendChild(pagename)
            headline.appendChild(pagenum)

            this.container.appendChild(headline)
            this.container.appendChild(container)
        } else {
            // @ts-ignore
            this.container.querySelector('h1 .pagename').innerText = this.pages.get(this.currentPage).name
            // @ts-ignore
            this.container.querySelector('h1 .pagenum').innerText = ` (${this.currentPage}/${this.pages.count()})`
        }

        if (this.currentPage === this.pages.count()) {
            this.container.classList.add('last-page')
        } else {
            this.container.classList.remove('last-page')
        }

        const page = this.pages.get(this.currentPage)

        this.isValid = null

        if (page) {
            // @ts-ignore
            this.hasMandatoryFields = page.render(container, valid => {
                if (valid === null || valid !== this.isValid) {
                    this.isValid = valid

                    if (this.btnNext) {
                        if (valid) {
                            this.btnNext.classList.remove('disabled')
                            this.btnSend.classList.remove('disabled')
                        } else {
                            this.btnNext.classList.add('disabled')
                            this.btnSend.classList.add('disabled')
                        }
                    }
                }
            })
        }
    }

    private updateFooter() {
        let container = this.container.querySelector('footer')

        if ( ! container) {
            container = document.createElement('footer')

            this.btnPrev = document.createElement('a')
            this.btnPrev.href = 'javascript:void(0);'
            this.btnPrev.classList.add('btn')
            this.btnPrev.classList.add('btn-prev')
            this.btnPrev.innerHTML = '<span class="arrow">&lt;</span> Zurück'

            this.btnNext = document.createElement('a')
            this.btnNext.href = 'javascript:void(0);'
            this.btnNext.classList.add('btn')
            this.btnNext.classList.add('btn-next')
            this.btnNext.innerHTML = 'Weiter <span class="arrow">&gt;</span>'

            this.btnSend = document.createElement('a')
            this.btnSend.href = 'javascript:void(0);'
            this.btnSend.classList.add('btn')
            this.btnSend.classList.add('btn-send')
            this.btnSend.innerHTML = 'Formular absenden'

            this.btnPrev.addEventListener('click', () => this.movePage(-1))
            this.btnNext.addEventListener('click', () => this.movePage(+1))
            this.btnSend.addEventListener('click', () => this.sendData())

            const buttonContainer = document.createElement('div')
            buttonContainer.classList.add('buttons')

            this.noticeMandatory = document.createElement('div')
            this.noticeMandatory.classList.add('notice-mandatory')
            this.noticeMandatory.innerHTML = '<span>*</span> Pflichtfelder'

            buttonContainer.appendChild(this.btnPrev)
            buttonContainer.appendChild(this.btnNext)
            buttonContainer.appendChild(this.btnSend)

            container.appendChild(buttonContainer)
            container.appendChild(this.noticeMandatory)

            this.container.appendChild(container)
        }

        if (this.currentPage === 1) {
            this.btnPrev.classList.add('disabled')
            this.btnNext.classList.remove('disabled')
            this.btnSend.classList.remove('disabled')
        }
        else if (this.currentPage === this.pages.count()) {
            this.btnPrev.classList.remove('disabled')
            this.btnNext.classList.add('disabled')
            this.btnSend.classList.add('disabled')
        }
        else {
            this.btnPrev.classList.remove('disabled')
            this.btnNext.classList.remove('disabled')
            this.btnSend.classList.remove('disabled')
        }

        if (this.isValid === false) {
            this.btnNext.classList.add('disabled')
            this.btnSend.classList.add('disabled')
        } else {
            this.btnNext.classList.remove('disabled')
            this.btnSend.classList.remove('disabled')
        }

        if (this.hasMandatoryFields) {
            this.noticeMandatory.classList.remove('hidden')
        } else {
            this.noticeMandatory.classList.add('hidden')
        }
    }

    private sendData() {
        const { fields, data } = store

        const pages = []
        for (const page of this.pages.all()) {
            const fieldsByConfig = page.config.map(c => {
                const res = []
                for (let field of c.fields) {
                    res.push(field.key)

                    if ('undefined' !== typeof field.combine && field.combine) {
                        for (let childField of field.combine.items) {
                            res.push(childField.key)
                        }
                    }
                }

                return res
            })

            let fields = []
            for (let c of fieldsByConfig) {
                fields = [...fields, ...c]
            }

            pages.push({
                sort: page.sort,
                name: page.name,
                slug: page.slug,
                fields
            })
        }

        const request = { name: this.name, pages, fields, data }

        this.setSending()

        fetch(`${this.backendUrl}?submit=1`, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then(res => {
                this.setSending(false)

                if ('undefined' === typeof res.success || ! res.success) {
                    return alert(`Leider ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.`)
                }

                const page = this.container.querySelector('.page')
                page.classList.add('completed')

                page.innerHTML = '<h1>Vielen Dank.</h1><h2>Wir haben Ihre Anfrage erhalten und melden uns schnellstmöglich bei Ihnen.</h2>'

                this.btnPrev.remove()
                this.btnSend.remove()
                this.noticeMandatory.remove()

                this.container.querySelector('.pagename').remove()
                this.container.querySelector('.pagenum').remove()

                sessionStorage.removeItem('pagenum')
                sessionStorage.removeItem('sf-form-data')

            })
            .catch(() => {
                alert(`Leider ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.`)
                this.setSending(false)
            })
    }

    private setSending(sending: boolean = true) {
        if (sending) {
            this.container.querySelector('.page').classList.add('sending')
            this.btnPrev.classList.add('disabled')
            this.btnSend.classList.add('disabled')
        } else {
            this.container.querySelector('.page').classList.remove('sending')
            this.btnPrev.classList.remove('disabled')
            this.btnSend.classList.remove('disabled')
        }
    }

    private movePage(step: number) {
        if (
            this.currentPage + step < 0
            || this.currentPage + step > this.pages.count()
        ) return

        this.currentPage += step

        sessionStorage.setItem('pagenum', '' + this.currentPage)

        this.update()
    }

    private updateHeader() {
        let container = this.container.querySelector('header')

        if ( ! container) {
            container = this.renderHeader()
        }

        container.querySelectorAll('li').forEach(item => {
            if (item.hasAttribute('data-page')) {
                const sort = parseInt(item.getAttribute('data-page'))
                if (sort <= this.currentPage) {
                    item.classList.add('active')
                } else {
                    item.classList.remove('active')
                }
            }
        })
    }
}
