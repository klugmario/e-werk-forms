import Page from "./Page";

export default class PageList {
    private pages: Page[] = []

    public add(page: Page): PageList {
        if ( ! page.sort) {
            const maxSort = Math.max(
                ...this.pages.map(p => p.sort)
            )

            page.sort = maxSort + 1
        }

        this.pages.push(page)

        return this
    }

    public all(): Page[] {
        return this.pages.sort((p1, p2) => p1.sort == p2.sort ? 0 : p1.sort > p2.sort ? 1 : -1)
    }

    get(sort: number) {
        const pages = this.all()

        return pages[sort - 1] || null
    }

    count() {
        return this.pages.length;
    }
}
