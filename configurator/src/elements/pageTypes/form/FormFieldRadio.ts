import {IFormField} from "../FormPage";
import store from "../../../store";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldRadioOption {
    key: string,
    label: string,
    help?: string
}

export interface IFormFieldRadio extends IFormField {
    options: IFormFieldRadioOption[]
    validations?: string[];
    help?: string;
}

export default class FormFieldRadio extends AbstractFormField implements IFormFieldRadio {
    type: string;
    key: string;
    label: string;
    required: boolean = false
    options: IFormFieldRadioOption[] = [];

    private value: string;
    private mobile: number = 0;

    protected isValid(value: any): boolean {
        return !! value;
    }

    protected getValue(): any {
        return this.value
    }

    create(container: HTMLDivElement, validCallback: (valid: boolean) => void): HTMLDivElement {
        const label = document.createElement('label')
        label.setAttribute('for', this.key)
        label.innerText = this.label

        if (this.required) {
            label.classList.add('required')
        }

        const boxes = document.createElement('ul')
        boxes.classList.add('box-list')

        if (this.mobile) {
            boxes.classList.add(`mobile-break-${this.mobile}`)
        }

        for (let option of this.options) {
            const boxLabel = document.createElement('label')
            boxLabel.setAttribute('for', option.key)
            boxLabel.innerText = option.label

            const input = document.createElement('input')
            input.id = option.key;
            input.setAttribute('type', 'radio')
            input.setAttribute('name', this.key)
            input.setAttribute('value', option.key)

            if ('undefined' !== typeof store.data[this.key] && store.data[this.key] === option.key) {
                input.checked = true
                this.value = option.key
            }

            input.addEventListener('change', () => {
                this.value = option.key
                this.onChange()
            })

            let help = null
            if (option.help) {
                help = document.createElement('a')
                help.classList.add('help')
                help.innerText = 'i'

                const helpPopup = document.createElement('div')
                helpPopup.innerText = option.help

                help.appendChild(helpPopup)
            }

            const wrapper = document.createElement('li')

            wrapper.appendChild(input)
            wrapper.appendChild(boxLabel)

            if (help) {
                wrapper.appendChild(help)
            }

            boxes.appendChild(wrapper)
        }

        const group = document.createElement('div')
        group.classList.add('input-group')

        group.appendChild(label)
        group.appendChild(boxes)

        this.renderCombined(group, validCallback)

        container.appendChild(group)

        this.validate()

        return group
    }

    static fromJson(data: IFormFieldRadio): FormFieldRadio {
        const field = new FormFieldRadio()
        field.type = data.type
        field.key = data.key
        field.label = data.label
        field.options = data.options

        if ('combine' in data) {
            field.combine = data.combine
        }
        if ('required' in data) {
            field.required = data.required
        }
        if ('validations' in data) {
            field.validations = data.validations
        }
        if ('help' in data) {
            field.help = data.help
        }
        if ('mobile' in data) {
            field.mobile = data.mobile
        }

        field.loaded()

        return field
    }

    protected getValueMapping(): Object | null {
        const res = {}
        for (let c of this.options) {
            res[c.key] = c.label
        }
        return res;
    }
}
