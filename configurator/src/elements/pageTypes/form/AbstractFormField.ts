import FormPage, {ICombinedFieldsConfig, IFormField} from "../FormPage";
import store from "../../../store";
import FormFieldText, {IFormFieldText} from "./FormFieldText";

export default abstract class AbstractFormField implements IFormField {
    combine: ICombinedFieldsConfig;
    key: string;
    label: string;
    required: boolean;
    type: string;
    help?: string

    protected container?: HTMLDivElement
    protected elem?: HTMLDivElement
    public validations: string[] = []

    private validCallback: (valid: boolean) => null;

    private checkValidations() {
        for (let val of this.validations) {
            if ('min-' === val.substr(0, 4)) {
                const [minType, minVal] = val.substr(4).split('-')

                if ( ! minVal) {
                    throw new Error(`missing min length for field validation "${this.key}"`)
                }

                switch (minType) {
                    case 'len':
                        return this.getValue().length >= minVal
                    default:
                        throw new Error(`invalid "min" validation type: "${minType}" for "${this.key}"`)
                }
            }

            switch (val) {
                default:
                    throw new Error(`unknown validation rule: ${val}`)
            }
        }

        return true
    }

    public loaded() {
        const mapping = this.getValueMapping()

        store.fields[this.key] = {
            label: this.label,
            mapping
        }
    }

    protected validate() {
        const value = this.getValue()

        if ( ! this.required) {
            this.validCallback(true)
            this.elem.classList.remove('invalid')
            return
        }

        let valid = this.checkValidations()

        if (valid && ! this.isValid(value)) {
            valid = false
        }

        this.validCallback(valid)

        if (this.elem) {
            if (valid) {
                this.elem.classList.remove('invalid')
            } else {
                this.elem.classList.add('invalid')
            }
        }
    }

    protected onChange() {
        store.data[this.key] = this.getValue()
        this.validate()
    }

    protected abstract getValue(): any
    protected abstract isValid(value: any): boolean
    protected abstract create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement

    public render(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        this.container = container
        this.validCallback = validCallback

        this.elem = this.create(container, validCallback)

        this.validate()

        return this.elem
    }

    public reload(): HTMLDivElement {
        this.elem = this.create(this.container, this.validCallback)
        return this.elem
    }

    renderCombined(group: HTMLDivElement, validCallback: (valid: boolean) => void) {
        if (this.combine) {
            group.classList.add('is-combined')

            const container = document.createElement('div')
            container.classList.add('group')

            if (this.combine.split) {
                container.classList.add('can-split')
            }

            group.appendChild(container)

            const processed = []

            let i = 0
            group.childNodes.forEach(item => {
                i++
                if (i < 2) {
                    return
                }

                item.parentNode.removeChild(item)

                try {
                    container.appendChild(item)
                    processed.push(item)
                } catch (e) {
                    console.log('error in item:', item)
                    console.error(e)
                }
                i++
            })

            container.firstChild.style.width = this.combine.widths[0] + '%'
            container.firstChild.style.flex = this.combine.widths[0]

            let j = 1
            for (let fieldConfig of this.combine.items) {
                const field = FormPage.createFieldFromJson(fieldConfig)
                const child = field.render(container, validCallback)

                child.classList.add('combination')

                child.style.width = this.combine.widths[j] + '%'
                child.style.flex = this.combine.widths[j]
                j++
            }
        }
    }

    protected abstract getValueMapping(): Object | null
}
