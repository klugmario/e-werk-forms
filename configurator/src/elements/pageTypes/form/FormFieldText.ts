import {IFormField} from "../FormPage";
import store from "../../../store";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldText extends IFormField {
    help: string;
    validations?: string[];
    prefix?: string,
    placeholder?: string,
    inputType?: string
    minw?: number;
    hideLabel?: boolean;
}

export default class FormFieldText extends AbstractFormField implements IFormFieldText {
    type: string;
    key: string;
    label: string;
    inputType: string = 'text'
    required: boolean = false
    prefix?: string;
    placeholder?: string;
    help: string
    minw: number = 0;
    hideLabel: boolean = false;

    private input

    protected isValid(value: any): boolean {
        return value.length > 0
    }

    protected getValue(): any {
        return this.input.value
    }

    protected create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        let label = null

        if (this.label && ! this.hideLabel) {
            label = document.createElement('label')
            label.setAttribute('for', this.key)
            label.innerText = this.label

            if (this.required) {
                label.classList.add('required')
            }
        }

        this.input = document.createElement('input')
        this.input.id = this.key
        this.input.setAttribute('type', this.inputType)

        if (this.required) {
            this.input.setAttribute('required', 'required')
        }

        if (this.placeholder) {
            this.input.setAttribute('placeholder', this.placeholder)
        }

        if (this.minw) {
            this.input.style.minWidth = `${this.minw}px`
        }

        if ('undefined' !== typeof store.data[this.key]) {
            this.input.value = store.data[this.key]
        }

        this.input.addEventListener('keyup', () => this.onChange())
        this.input.addEventListener('change', () => this.onChange())

        const group = document.createElement('div')
        group.classList.add('input-group')

        if (label && ! this.hideLabel) {
            group.appendChild(label)
        } else {
            this.input.classList.add('no-label')
        }

        group.appendChild(this.input)

        this.renderCombined(group, validCallback)

        if (this.help) {
            const help = document.createElement('a')
            help.classList.add('help')
            help.innerText = 'i'

            const helpPopup = document.createElement('div')
            helpPopup.innerText = this.help

            help.appendChild(helpPopup)

            label?.appendChild(help)
        }

        if (this.prefix) {
            const prefix = document.createElement('span')
            prefix.classList.add('prefix')
            prefix.innerText = this.prefix

            this.input.parentElement.insertBefore(prefix, this.input)
        }

        container.appendChild(group)

        return group
    }

    static fromJson(data: IFormFieldText): FormFieldText {
        const field = new FormFieldText()
        field.type = data.type
        field.key = data.key
        field.label = data.label

        if ('prefix' in data) {
            field.prefix = data.prefix
        }
        if ('combine' in data) {
            field.combine = data.combine
        }
        if ('required' in data) {
            field.required = data.required
        }
        if ('inputType' in data) {
            field.inputType = data.inputType
        }
        if ('placeholder' in data) {
            field.placeholder = data.placeholder
        }
        if ('validations' in data) {
            field.validations = data.validations
        }
        if ('help' in data) {
            field.help = data.help
        }
        if ('minw' in data) {
            field.minw = data.minw
        }
        if ('hideLabel' in data) {
            field.hideLabel = data.hideLabel
        }

        field.loaded()

        return field
    }

    protected getValueMapping(): Object | null {
        return null;
    }
}
