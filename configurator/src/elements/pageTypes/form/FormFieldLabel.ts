import {IFormField} from "../FormPage";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldLabel extends IFormField {
    align: string;
}

export default class FormFieldLabel extends AbstractFormField implements IFormFieldLabel {
    type: string;
    key: string;
    label: string;
    required: boolean = false
    align: string = 'center';

    protected getValue() {
        return null;
    }

    protected getValueMapping(): Object | null {
        return null;
    }

    protected isValid(value: any): boolean {
        return true;
    }

    protected create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        const label = document.createElement('div')

        label.classList.add('message')
        label.innerHTML = this.label
        label.style.textAlign = this.align

        container.appendChild(label)

        return label
    }

    static fromJson(data: IFormFieldLabel): FormFieldLabel {
        const field = new FormFieldLabel()
        field.type = data.type
        field.key = data.key
        field.label = data.label

        if ('combine' in data) {
            field.combine = data.combine
        }
        if ('align' in data) {
            field.align = data.align
        }

        return field
    }
}
