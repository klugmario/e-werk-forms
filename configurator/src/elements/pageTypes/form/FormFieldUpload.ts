import {IFormField} from "../FormPage";
import store from "../../../store";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldUpload extends IFormField {
    validations?: string[];
    prefix?: string,
    placeholder?: string,
    inputType?: string
}

export default class FormFieldUpload extends AbstractFormField implements IFormFieldUpload {
    type: string;
    key: string;
    label: string;
    inputType: string = 'text'
    required: boolean = false
    prefix?: string;
    placeholder?: string;

    private input
    private file?: {name: string, size: number, content: string | ArrayBuffer}

    protected isValid(value: any): boolean {
        return !! value
    }

    protected getValue(): any {
        return this.file
    }

    protected create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        if (this.key in store.data) {
            this.file = store.data[this.key]
        }

        let label = null

        if (this.label) {
            label = document.createElement('label')
            label.setAttribute('for', this.key)
            label.innerText = this.label

            if (this.required) {
                label.classList.add('required')
            }
        }

        let input

        if (this.file) {
            input = document.createElement('div')
            input.classList.add('uploaded-file')

            const filename = document.createElement('a')
            filename.classList.add('filename')
            filename.innerText = this.file.name
            filename.download = this.file.name
            filename.href = this.file.content.toString()
            filename.setAttribute('target', '_blank')

            const removeLink = document.createElement('a')
            removeLink.href = 'javascript:void(0)'
            removeLink.innerText = 'ändern'

            removeLink.addEventListener('click', () => {
                this.file = null

                this.onChange()
                this.reload()
            })

            input.appendChild(filename)
            input.appendChild(removeLink)
        } else {
            this.input = document.createElement('input')
            this.input.id = this.key
            this.input.setAttribute('type', 'file')

            if (this.required) {
                this.input.setAttribute('required', 'required')
            }

            if (this.placeholder) {
                this.input.setAttribute('placeholder', this.placeholder)
            }

            const onFieldChange = () => {
                const file = this.input.files[0]

                if (file) {
                    const reader = new FileReader()
                    reader.addEventListener('load', (e) => {
                        this.file = {
                            name: file.name,
                            size: file.size,
                            content: reader.result
                        }

                        this.onChange()
                        this.reload()
                    })
                    reader.readAsDataURL(file)
                }
            }

            this.input.addEventListener('change', onFieldChange)

            input = this.input
        }

        const group = document.createElement('div')
        group.classList.add('input-group')

        if (label) {
            group.appendChild(label)
        } else {
            input.classList.add('no-label')
        }

        group.appendChild(input)

        this.renderCombined(group, validCallback)

        if (this.prefix) {
            const prefix = document.createElement('span')
            prefix.classList.add('prefix')
            prefix.innerText = this.prefix

            this.input.parentElement.insertBefore(prefix, this.input)
        }

        if ( ! this.file) {
            group.classList.add('invalid')
        }

        if (this.elem && this.elem.isConnected) {
            this.elem.parentNode.insertBefore(group, this.elem)
            this.elem.parentNode.removeChild(this.elem)
        } else {
            container.appendChild(group)
        }

        return group
    }

    static fromJson(data: IFormFieldUpload): FormFieldUpload {
        const field = new FormFieldUpload()
        field.type = data.type
        field.key = data.key
        field.label = data.label

        if ('prefix' in data) {
            field.prefix = data.prefix
        }
        if ('combine' in data) {
            field.combine = data.combine
        }
        if ('required' in data) {
            field.required = data.required
        }
        if ('inputType' in data) {
            field.inputType = data.inputType
        }
        if ('placeholder' in data) {
            field.placeholder = data.placeholder
        }
        if ('validations' in data) {
            field.validations = data.validations
        }

        field.loaded()

        return field
    }

    protected getValueMapping(): Object | null {
        return null;
    }
}
