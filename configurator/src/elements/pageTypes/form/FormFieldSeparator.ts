import {IFormField} from "../FormPage";
import store from "../../../store";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldSeparator extends IFormField {
}

export default class FormFieldSeparator extends AbstractFormField implements IFormFieldSeparator {
    protected isValid(value: any): boolean {
        return true
    }

    protected getValue(): any {
        return null
    }

    protected create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        const hr = document.createElement('hr')

        container.appendChild(hr)

        return hr
    }

    static fromJson(data: IFormFieldSeparator): FormFieldSeparator {
        const field = new FormFieldSeparator()

        field.loaded()

        return field
    }

    protected getValueMapping(): Object | null {
        return null;
    }
}
