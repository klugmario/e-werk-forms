import {IFormField} from "../FormPage";
import store from "../../../store";
import AbstractFormField from "./AbstractFormField";

export interface IFormFieldCheckbox extends IFormField {
    validations?: string[];
    prefix?: string,
    placeholder?: string,
    inputType?: string
}

export default class FormFieldCheckbox extends AbstractFormField implements IFormFieldCheckbox {
    type: string;
    key: string;
    label: string;
    required: boolean = false
    prefix?: string;
    placeholder?: string;

    private input

    protected isValid(value: any): boolean {
        return value
    }

    protected getValue(): any {
        return !! this.input.checked
    }

    protected create(container: HTMLDivElement, validCallback: (valid: boolean) => null): HTMLDivElement {
        let label = null

        if (this.label) {
            label = document.createElement('label')
            label.setAttribute('for', this.key)
            label.innerText = this.label

            if (this.required) {
                label.classList.add('required')
            }
        }

        this.input = document.createElement('input')
        this.input.id = this.key
        this.input.setAttribute('type', 'checkbox')

        if (this.required) {
            this.input.setAttribute('required', 'required')
        }

        if (this.placeholder) {
            this.input.setAttribute('placeholder', this.placeholder)
        }

        if ('undefined' !== typeof store.data[this.key]) {
            this.input.checked = store.data[this.key]
        }

        this.input.addEventListener('change', () => this.onChange())

        const group = document.createElement('div')
        group.classList.add('input-group')

        if (label) {
            group.appendChild(label)
        } else {
            this.input.classList.add('no-label')
        }

        const wrap = document.createElement('div')
        wrap.classList.add('cb-wrap')

        wrap.appendChild(this.input)
        wrap.appendChild(document.createElement('i'))

        group.appendChild(wrap)

        this.renderCombined(group, validCallback)

        if (this.prefix) {
            const prefix = document.createElement('span')
            prefix.classList.add('prefix')
            prefix.innerText = this.prefix

            this.input.parentElement.insertBefore(prefix, this.input)
        }

        container.appendChild(group)

        return group
    }

    static fromJson(data: IFormFieldCheckbox): FormFieldCheckbox {
        const field = new FormFieldCheckbox()
        field.type = data.type
        field.key = data.key
        field.label = data.label

        if ('prefix' in data) {
            field.prefix = data.prefix
        }
        if ('combine' in data) {
            field.combine = data.combine
        }
        if ('required' in data) {
            field.required = data.required
        }
        if ('placeholder' in data) {
            field.placeholder = data.placeholder
        }
        if ('validations' in data) {
            field.validations = data.validations
        }

        field.loaded()

        return field
    }

    protected getValueMapping(): Object | null {
        return null;
    }
}
