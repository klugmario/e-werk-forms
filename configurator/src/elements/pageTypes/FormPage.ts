import FormFieldRadio, {IFormFieldRadio} from "./form/FormFieldRadio";
import FormFieldText, {IFormFieldText} from "./form/FormFieldText";
import FormFieldLabel, {IFormFieldLabel} from "./form/FormFieldLabel";
import AbstractFormField from "./form/AbstractFormField";
import FormFieldUpload, {IFormFieldUpload} from "./form/FormFieldUpload";
import FormFieldCheckbox from "./form/FormFieldCheckbox";
import FormFieldSeparator, {IFormFieldSeparator} from "./form/FormFieldSeparator";
import FormFieldTextarea, {IFormFieldTextarea} from "./form/FormFieldTextarea";

export interface ICombinedFieldsConfig {
    widths: number[],
    split: boolean,
    items: Array<IFormFieldRadio | IFormFieldText | IFormFieldLabel>
}

export interface IFormField {
    label: string,
    type: string,
    key?: string,
    required?: boolean,
    combine?: ICombinedFieldsConfig,
    render?: (HTMLDivElement, validCallback: (valid: boolean) => null) => HTMLElement
}

export interface IPageConfigForm {
    type: string,
    fields: AbstractFormField[]
}

export default class FormPage implements IPageConfigForm {
    fields: AbstractFormField[] = []
    type: 'form';

    public render(container: HTMLDivElement, validCallback: (invalidFields: string[]) => void): boolean {
        const block = document.createElement('section')

        let hasMandatoryFields = false

        const validity = new Proxy({}, {
            set: (obj, key, value) => {
                obj[key] = value

                const invalidFields = []
                for (let k in obj) {
                    if ( ! obj[k]) {
                        invalidFields.push(k)
                    }
                }

                validCallback(invalidFields)

                return true
            }
        })

        for (let field of this.fields) {
            if ('undefined' === typeof field.render) {
                console.error('form field has no render method', field)
                throw new Error('form field has no render method')
            }

            if (field.required) {
                hasMandatoryFields = true
            }

            field.render(block, valid => {
                validity[field.key] = valid
            })
        }

        container.appendChild(block)

        return hasMandatoryFields
    }

    static fromJson(c: IPageConfigForm): FormPage {
        const page = new FormPage()

        for (let fieldConfig of c.fields) {
            const field = FormPage.createFieldFromJson(fieldConfig)

            // @ts-ignore
            page.fields.push(field)
        }

        return page;
    }

    public static createFieldFromJson(fieldConfig: IFormFieldRadio | IFormFieldText | IFormFieldLabel) {
        switch (fieldConfig.type) {
            case 'radio':
                // @ts-ignore
                return FormFieldRadio.fromJson(fieldConfig as IFormFieldRadio)
            case 'text':
                // @ts-ignore
                return FormFieldText.fromJson(fieldConfig as IFormFieldText)
            case 'textarea':
                // @ts-ignore
                return FormFieldTextarea.fromJson(fieldConfig as IFormFieldTextarea)
            case 'checkbox':
                // @ts-ignore
                return FormFieldCheckbox.fromJson(fieldConfig as IFormFieldCheckbox)
            case 'label':
                // @ts-ignore
                return FormFieldLabel.fromJson(fieldConfig as IFormFieldLabel)
            case 'upload':
                // @ts-ignore
                return FormFieldUpload.fromJson(fieldConfig as IFormFieldUpload)
            case 'separator':
                // @ts-ignore
                return FormFieldSeparator.fromJson(fieldConfig as IFormFieldSeparator)
            default:
                throw new Error('unknown form field type: ' + fieldConfig.type)
        }
    }
}
