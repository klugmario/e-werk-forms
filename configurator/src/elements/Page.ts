import {slugify} from "../functions";
import FormPage, {IPageConfigForm} from "./pageTypes/FormPage";

export interface IPage {
    sort: number,
    name: string,
    slug: string,
    config: Array<IPageConfigForm>
}

export default class Page implements IPage
{
    sort: number = 0
    slug: string
    name: string
    config: IPageConfigForm[] = []

    constructor(name: string) {
        this.name = name
        this.slug = slugify(name)
    }

    static fromJson(pageData: IPage) {
        for (let key of ['sort', 'slug', 'name']) {
            if ('undefined' === typeof pageData[key]) {
                throw new Error('page is missing "sort" key')
            }
        }

        const page = new Page(pageData.name)
        page.sort = pageData.sort
        page.slug = pageData.slug

        for (let c of pageData.config) {
            switch (c.type) {
                case 'form':
                    page.config.push(
                        FormPage.fromJson(c)
                    )
                    break
                default:
                    throw new Error('unknown page config type: ' + c.type)
            }
        }

        return page
    }

    render(container: HTMLDivElement, validCallback: (valid: boolean) => void): boolean {
        container.innerHTML = ''

        const sending = document.createElement('div')
        sending.classList.add('sending-layer')
        sending.innerHTML = '<h1>Senden...</h1>';
        container.appendChild(sending)

        let hasMandatoryFields = false

        let lastInvalidFields = []

        const validConfigs = new Proxy({}, {
            set: (obj, key, value) => {
                obj[key] = value

                const invalidFields = []
                for (let config in obj) {
                    for (let key of obj[config]) {
                        invalidFields.push(key)
                    }
                }

                if ( ! lastInvalidFields.length || lastInvalidFields.join('-') !== invalidFields.join('-')) {
                    lastInvalidFields = [...invalidFields]

                    validCallback(invalidFields.length < 1)
                }
                return true
            }
        })

        for (let c of this.config) {
            // @ts-ignore
            if (c.render(container, valid => validConfigs[c] = valid)) {
                hasMandatoryFields = true
            }
        }

        return hasMandatoryFields
    }
}
