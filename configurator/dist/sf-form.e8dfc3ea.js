// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"src/elements/PageList.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var PageList =
/** @class */
function () {
  function PageList() {
    this.pages = [];
  }

  PageList.prototype.add = function (page) {
    if (!page.sort) {
      var maxSort = Math.max.apply(Math, this.pages.map(function (p) {
        return p.sort;
      }));
      page.sort = maxSort + 1;
    }

    this.pages.push(page);
    return this;
  };

  PageList.prototype.all = function () {
    return this.pages.sort(function (p1, p2) {
      return p1.sort == p2.sort ? 0 : p1.sort > p2.sort ? 1 : -1;
    });
  };

  PageList.prototype.get = function (sort) {
    var pages = this.all();
    return pages[sort - 1] || null;
  };

  PageList.prototype.count = function () {
    return this.pages.length;
  };

  return PageList;
}();

exports.default = PageList;
},{}],"src/functions.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.slugify = void 0;

var slugify = function slugify(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim

  str = str.toLowerCase();
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to = "aaaaeeeeiiiioooouuuunc------";

  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-');
  return str;
};

exports.slugify = slugify;
},{}],"src/store.ts":[function(require,module,exports) {
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
var storedData = sessionStorage.getItem('sf-form-data');
var data = storedData ? JSON.parse(storedData) : {
  fields: {},
  data: {}
};
var handler = {
  get: function get(target, key) {
    if (_typeof(target[key]) === 'object' && target[key] !== null) {
      return new Proxy(target[key], handler);
    } else {
      return target[key];
    }
  },
  set: function set(obj, prop, value) {
    obj[prop] = value;
    setTimeout(function () {
      sessionStorage.setItem('sf-form-data', JSON.stringify(data));
    }, 100);
    return true;
  }
}; // @ts-ignore

var store = new Proxy(data, handler);
exports.default = store;
},{}],"src/elements/pageTypes/form/AbstractFormField.ts":[function(require,module,exports) {
"use strict";

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var FormPage_1 = __importDefault(require("../FormPage"));

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField =
/** @class */
function () {
  function AbstractFormField() {
    this.validations = [];
  }

  AbstractFormField.prototype.checkValidations = function () {
    for (var _i = 0, _a = this.validations; _i < _a.length; _i++) {
      var val = _a[_i];

      if ('min-' === val.substr(0, 4)) {
        var _b = val.substr(4).split('-'),
            minType = _b[0],
            minVal = _b[1];

        if (!minVal) {
          throw new Error("missing min length for field validation \"" + this.key + "\"");
        }

        switch (minType) {
          case 'len':
            return this.getValue().length >= minVal;

          default:
            throw new Error("invalid \"min\" validation type: \"" + minType + "\" for \"" + this.key + "\"");
        }
      }

      switch (val) {
        default:
          throw new Error("unknown validation rule: " + val);
      }
    }

    return true;
  };

  AbstractFormField.prototype.loaded = function () {
    var mapping = this.getValueMapping();
    store_1.default.fields[this.key] = {
      label: this.label,
      mapping: mapping
    };
  };

  AbstractFormField.prototype.validate = function () {
    var value = this.getValue();

    if (!this.required) {
      this.validCallback(true);
      this.elem.classList.remove('invalid');
      return;
    }

    var valid = this.checkValidations();

    if (valid && !this.isValid(value)) {
      valid = false;
    }

    this.validCallback(valid);

    if (this.elem) {
      if (valid) {
        this.elem.classList.remove('invalid');
      } else {
        this.elem.classList.add('invalid');
      }
    }
  };

  AbstractFormField.prototype.onChange = function () {
    store_1.default.data[this.key] = this.getValue();
    this.validate();
  };

  AbstractFormField.prototype.render = function (container, validCallback) {
    this.container = container;
    this.validCallback = validCallback;
    this.elem = this.create(container, validCallback);
    this.validate();
    return this.elem;
  };

  AbstractFormField.prototype.reload = function () {
    this.elem = this.create(this.container, this.validCallback);
    return this.elem;
  };

  AbstractFormField.prototype.renderCombined = function (group, validCallback) {
    if (this.combine) {
      group.classList.add('is-combined');
      var container_1 = document.createElement('div');
      container_1.classList.add('group');

      if (this.combine.split) {
        container_1.classList.add('can-split');
      }

      group.appendChild(container_1);
      var processed_1 = [];
      var i_1 = 0;
      group.childNodes.forEach(function (item) {
        i_1++;

        if (i_1 < 2) {
          return;
        }

        item.parentNode.removeChild(item);

        try {
          container_1.appendChild(item);
          processed_1.push(item);
        } catch (e) {
          console.log('error in item:', item);
          console.error(e);
        }

        i_1++;
      });
      container_1.firstChild.style.width = this.combine.widths[0] + '%';
      container_1.firstChild.style.flex = this.combine.widths[0];
      var j = 1;

      for (var _i = 0, _a = this.combine.items; _i < _a.length; _i++) {
        var fieldConfig = _a[_i];
        var field = FormPage_1.default.createFieldFromJson(fieldConfig);
        var child = field.render(container_1, validCallback);
        child.classList.add('combination');
        child.style.width = this.combine.widths[j] + '%';
        child.style.flex = this.combine.widths[j];
        j++;
      }
    }
  };

  return AbstractFormField;
}();

exports.default = AbstractFormField;
},{"../FormPage":"src/elements/pageTypes/FormPage.ts","../../../store":"src/store.ts"}],"src/elements/pageTypes/form/FormFieldRadio.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldRadio =
/** @class */
function (_super) {
  __extends(FormFieldRadio, _super);

  function FormFieldRadio() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.required = false;
    _this.options = [];
    _this.mobile = 0;
    return _this;
  }

  FormFieldRadio.prototype.isValid = function (value) {
    return !!value;
  };

  FormFieldRadio.prototype.getValue = function () {
    return this.value;
  };

  FormFieldRadio.prototype.create = function (container, validCallback) {
    var _this = this;

    var label = document.createElement('label');
    label.setAttribute('for', this.key);
    label.innerText = this.label;

    if (this.required) {
      label.classList.add('required');
    }

    var boxes = document.createElement('ul');
    boxes.classList.add('box-list');

    if (this.mobile) {
      boxes.classList.add("mobile-break-" + this.mobile);
    }

    var _loop_1 = function _loop_1(option) {
      var boxLabel = document.createElement('label');
      boxLabel.setAttribute('for', option.key);
      boxLabel.innerText = option.label;
      var input = document.createElement('input');
      input.id = option.key;
      input.setAttribute('type', 'radio');
      input.setAttribute('name', this_1.key);
      input.setAttribute('value', option.key);

      if ('undefined' !== typeof store_1.default.data[this_1.key] && store_1.default.data[this_1.key] === option.key) {
        input.checked = true;
        this_1.value = option.key;
      }

      input.addEventListener('change', function () {
        _this.value = option.key;

        _this.onChange();
      });
      var help = null;

      if (option.help) {
        help = document.createElement('a');
        help.classList.add('help');
        help.innerText = 'i';
        var helpPopup = document.createElement('div');
        helpPopup.innerText = option.help;
        help.appendChild(helpPopup);
      }

      var wrapper = document.createElement('li');
      wrapper.appendChild(input);
      wrapper.appendChild(boxLabel);

      if (help) {
        wrapper.appendChild(help);
      }

      boxes.appendChild(wrapper);
    };

    var this_1 = this;

    for (var _i = 0, _a = this.options; _i < _a.length; _i++) {
      var option = _a[_i];

      _loop_1(option);
    }

    var group = document.createElement('div');
    group.classList.add('input-group');
    group.appendChild(label);
    group.appendChild(boxes);
    this.renderCombined(group, validCallback);
    container.appendChild(group);
    this.validate();
    return group;
  };

  FormFieldRadio.fromJson = function (data) {
    var field = new FormFieldRadio();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;
    field.options = data.options;

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('required' in data) {
      field.required = data.required;
    }

    if ('validations' in data) {
      field.validations = data.validations;
    }

    if ('help' in data) {
      field.help = data.help;
    }

    if ('mobile' in data) {
      field.mobile = data.mobile;
    }

    field.loaded();
    return field;
  };

  FormFieldRadio.prototype.getValueMapping = function () {
    var res = {};

    for (var _i = 0, _a = this.options; _i < _a.length; _i++) {
      var c = _a[_i];
      res[c.key] = c.label;
    }

    return res;
  };

  return FormFieldRadio;
}(AbstractFormField_1.default);

exports.default = FormFieldRadio;
},{"../../../store":"src/store.ts","./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldText.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldText =
/** @class */
function (_super) {
  __extends(FormFieldText, _super);

  function FormFieldText() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.inputType = 'text';
    _this.required = false;
    _this.minw = 0;
    _this.hideLabel = false;
    return _this;
  }

  FormFieldText.prototype.isValid = function (value) {
    return value.length > 0;
  };

  FormFieldText.prototype.getValue = function () {
    return this.input.value;
  };

  FormFieldText.prototype.create = function (container, validCallback) {
    var _this = this;

    var label = null;

    if (this.label && !this.hideLabel) {
      label = document.createElement('label');
      label.setAttribute('for', this.key);
      label.innerText = this.label;

      if (this.required) {
        label.classList.add('required');
      }
    }

    this.input = document.createElement('input');
    this.input.id = this.key;
    this.input.setAttribute('type', this.inputType);

    if (this.required) {
      this.input.setAttribute('required', 'required');
    }

    if (this.placeholder) {
      this.input.setAttribute('placeholder', this.placeholder);
    }

    if (this.minw) {
      this.input.style.minWidth = this.minw + "px";
    }

    if ('undefined' !== typeof store_1.default.data[this.key]) {
      this.input.value = store_1.default.data[this.key];
    }

    this.input.addEventListener('keyup', function () {
      return _this.onChange();
    });
    this.input.addEventListener('change', function () {
      return _this.onChange();
    });
    var group = document.createElement('div');
    group.classList.add('input-group');

    if (label && !this.hideLabel) {
      group.appendChild(label);
    } else {
      this.input.classList.add('no-label');
    }

    group.appendChild(this.input);
    this.renderCombined(group, validCallback);

    if (this.help) {
      var help = document.createElement('a');
      help.classList.add('help');
      help.innerText = 'i';
      var helpPopup = document.createElement('div');
      helpPopup.innerText = this.help;
      help.appendChild(helpPopup);
      label === null || label === void 0 ? void 0 : label.appendChild(help);
    }

    if (this.prefix) {
      var prefix = document.createElement('span');
      prefix.classList.add('prefix');
      prefix.innerText = this.prefix;
      this.input.parentElement.insertBefore(prefix, this.input);
    }

    container.appendChild(group);
    return group;
  };

  FormFieldText.fromJson = function (data) {
    var field = new FormFieldText();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;

    if ('prefix' in data) {
      field.prefix = data.prefix;
    }

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('required' in data) {
      field.required = data.required;
    }

    if ('inputType' in data) {
      field.inputType = data.inputType;
    }

    if ('placeholder' in data) {
      field.placeholder = data.placeholder;
    }

    if ('validations' in data) {
      field.validations = data.validations;
    }

    if ('help' in data) {
      field.help = data.help;
    }

    if ('minw' in data) {
      field.minw = data.minw;
    }

    if ('hideLabel' in data) {
      field.hideLabel = data.hideLabel;
    }

    field.loaded();
    return field;
  };

  FormFieldText.prototype.getValueMapping = function () {
    return null;
  };

  return FormFieldText;
}(AbstractFormField_1.default);

exports.default = FormFieldText;
},{"../../../store":"src/store.ts","./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldLabel.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldLabel =
/** @class */
function (_super) {
  __extends(FormFieldLabel, _super);

  function FormFieldLabel() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.required = false;
    _this.align = 'center';
    return _this;
  }

  FormFieldLabel.prototype.getValue = function () {
    return null;
  };

  FormFieldLabel.prototype.getValueMapping = function () {
    return null;
  };

  FormFieldLabel.prototype.isValid = function (value) {
    return true;
  };

  FormFieldLabel.prototype.create = function (container, validCallback) {
    var label = document.createElement('div');
    label.classList.add('message');
    label.innerHTML = this.label;
    label.style.textAlign = this.align;
    container.appendChild(label);
    return label;
  };

  FormFieldLabel.fromJson = function (data) {
    var field = new FormFieldLabel();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('align' in data) {
      field.align = data.align;
    }

    return field;
  };

  return FormFieldLabel;
}(AbstractFormField_1.default);

exports.default = FormFieldLabel;
},{"./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldUpload.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldUpload =
/** @class */
function (_super) {
  __extends(FormFieldUpload, _super);

  function FormFieldUpload() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.inputType = 'text';
    _this.required = false;
    return _this;
  }

  FormFieldUpload.prototype.isValid = function (value) {
    return !!value;
  };

  FormFieldUpload.prototype.getValue = function () {
    return this.file;
  };

  FormFieldUpload.prototype.create = function (container, validCallback) {
    var _this = this;

    if (this.key in store_1.default.data) {
      this.file = store_1.default.data[this.key];
    }

    var label = null;

    if (this.label) {
      label = document.createElement('label');
      label.setAttribute('for', this.key);
      label.innerText = this.label;

      if (this.required) {
        label.classList.add('required');
      }
    }

    var input;

    if (this.file) {
      input = document.createElement('div');
      input.classList.add('uploaded-file');
      var filename = document.createElement('a');
      filename.classList.add('filename');
      filename.innerText = this.file.name;
      filename.download = this.file.name;
      filename.href = this.file.content.toString();
      filename.setAttribute('target', '_blank');
      var removeLink = document.createElement('a');
      removeLink.href = 'javascript:void(0)';
      removeLink.innerText = 'ändern';
      removeLink.addEventListener('click', function () {
        _this.file = null;

        _this.onChange();

        _this.reload();
      });
      input.appendChild(filename);
      input.appendChild(removeLink);
    } else {
      this.input = document.createElement('input');
      this.input.id = this.key;
      this.input.setAttribute('type', 'file');

      if (this.required) {
        this.input.setAttribute('required', 'required');
      }

      if (this.placeholder) {
        this.input.setAttribute('placeholder', this.placeholder);
      }

      var onFieldChange = function onFieldChange() {
        var file = _this.input.files[0];

        if (file) {
          var reader_1 = new FileReader();
          reader_1.addEventListener('load', function (e) {
            _this.file = {
              name: file.name,
              size: file.size,
              content: reader_1.result
            };

            _this.onChange();

            _this.reload();
          });
          reader_1.readAsDataURL(file);
        }
      };

      this.input.addEventListener('change', onFieldChange);
      input = this.input;
    }

    var group = document.createElement('div');
    group.classList.add('input-group');

    if (label) {
      group.appendChild(label);
    } else {
      input.classList.add('no-label');
    }

    group.appendChild(input);
    this.renderCombined(group, validCallback);

    if (this.prefix) {
      var prefix = document.createElement('span');
      prefix.classList.add('prefix');
      prefix.innerText = this.prefix;
      this.input.parentElement.insertBefore(prefix, this.input);
    }

    if (!this.file) {
      group.classList.add('invalid');
    }

    if (this.elem && this.elem.isConnected) {
      this.elem.parentNode.insertBefore(group, this.elem);
      this.elem.parentNode.removeChild(this.elem);
    } else {
      container.appendChild(group);
    }

    return group;
  };

  FormFieldUpload.fromJson = function (data) {
    var field = new FormFieldUpload();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;

    if ('prefix' in data) {
      field.prefix = data.prefix;
    }

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('required' in data) {
      field.required = data.required;
    }

    if ('inputType' in data) {
      field.inputType = data.inputType;
    }

    if ('placeholder' in data) {
      field.placeholder = data.placeholder;
    }

    if ('validations' in data) {
      field.validations = data.validations;
    }

    field.loaded();
    return field;
  };

  FormFieldUpload.prototype.getValueMapping = function () {
    return null;
  };

  return FormFieldUpload;
}(AbstractFormField_1.default);

exports.default = FormFieldUpload;
},{"../../../store":"src/store.ts","./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldCheckbox.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldCheckbox =
/** @class */
function (_super) {
  __extends(FormFieldCheckbox, _super);

  function FormFieldCheckbox() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.required = false;
    return _this;
  }

  FormFieldCheckbox.prototype.isValid = function (value) {
    return value;
  };

  FormFieldCheckbox.prototype.getValue = function () {
    return !!this.input.checked;
  };

  FormFieldCheckbox.prototype.create = function (container, validCallback) {
    var _this = this;

    var label = null;

    if (this.label) {
      label = document.createElement('label');
      label.setAttribute('for', this.key);
      label.innerText = this.label;

      if (this.required) {
        label.classList.add('required');
      }
    }

    this.input = document.createElement('input');
    this.input.id = this.key;
    this.input.setAttribute('type', 'checkbox');

    if (this.required) {
      this.input.setAttribute('required', 'required');
    }

    if (this.placeholder) {
      this.input.setAttribute('placeholder', this.placeholder);
    }

    if ('undefined' !== typeof store_1.default.data[this.key]) {
      this.input.checked = store_1.default.data[this.key];
    }

    this.input.addEventListener('change', function () {
      return _this.onChange();
    });
    var group = document.createElement('div');
    group.classList.add('input-group');

    if (label) {
      group.appendChild(label);
    } else {
      this.input.classList.add('no-label');
    }

    var wrap = document.createElement('div');
    wrap.classList.add('cb-wrap');
    wrap.appendChild(this.input);
    wrap.appendChild(document.createElement('i'));
    group.appendChild(wrap);
    this.renderCombined(group, validCallback);

    if (this.prefix) {
      var prefix = document.createElement('span');
      prefix.classList.add('prefix');
      prefix.innerText = this.prefix;
      this.input.parentElement.insertBefore(prefix, this.input);
    }

    container.appendChild(group);
    return group;
  };

  FormFieldCheckbox.fromJson = function (data) {
    var field = new FormFieldCheckbox();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;

    if ('prefix' in data) {
      field.prefix = data.prefix;
    }

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('required' in data) {
      field.required = data.required;
    }

    if ('placeholder' in data) {
      field.placeholder = data.placeholder;
    }

    if ('validations' in data) {
      field.validations = data.validations;
    }

    field.loaded();
    return field;
  };

  FormFieldCheckbox.prototype.getValueMapping = function () {
    return null;
  };

  return FormFieldCheckbox;
}(AbstractFormField_1.default);

exports.default = FormFieldCheckbox;
},{"../../../store":"src/store.ts","./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldSeparator.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldSeparator =
/** @class */
function (_super) {
  __extends(FormFieldSeparator, _super);

  function FormFieldSeparator() {
    return _super !== null && _super.apply(this, arguments) || this;
  }

  FormFieldSeparator.prototype.isValid = function (value) {
    return true;
  };

  FormFieldSeparator.prototype.getValue = function () {
    return null;
  };

  FormFieldSeparator.prototype.create = function (container, validCallback) {
    var hr = document.createElement('hr');
    container.appendChild(hr);
    return hr;
  };

  FormFieldSeparator.fromJson = function (data) {
    var field = new FormFieldSeparator();
    field.loaded();
    return field;
  };

  FormFieldSeparator.prototype.getValueMapping = function () {
    return null;
  };

  return FormFieldSeparator;
}(AbstractFormField_1.default);

exports.default = FormFieldSeparator;
},{"./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/form/FormFieldTextarea.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = __importDefault(require("../../../store"));

var AbstractFormField_1 = __importDefault(require("./AbstractFormField"));

var FormFieldTextarea =
/** @class */
function (_super) {
  __extends(FormFieldTextarea, _super);

  function FormFieldTextarea() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.required = false;
    _this.minw = 0;
    _this.rows = 4;
    _this.hideLabel = false;
    return _this;
  }

  FormFieldTextarea.prototype.isValid = function (value) {
    return value.length > 0;
  };

  FormFieldTextarea.prototype.getValue = function () {
    return this.input.value;
  };

  FormFieldTextarea.prototype.create = function (container, validCallback) {
    var _this = this;

    var label = null;

    if (this.label && !this.hideLabel) {
      label = document.createElement('label');
      label.setAttribute('for', this.key);
      label.innerText = this.label;

      if (this.required) {
        label.classList.add('required');
      }
    }

    this.input = document.createElement('textarea');
    this.input.id = this.key;
    this.input.rows = this.rows;

    if (this.required) {
      this.input.setAttribute('required', 'required');
    }

    if (this.placeholder) {
      this.input.setAttribute('placeholder', this.placeholder);
    }

    if (this.minw) {
      this.input.style.minWidth = this.minw + "px";
    }

    if ('undefined' !== typeof store_1.default.data[this.key]) {
      this.input.value = store_1.default.data[this.key];
    }

    this.input.addEventListener('keyup', function () {
      return _this.onChange();
    });
    this.input.addEventListener('change', function () {
      return _this.onChange();
    });
    var group = document.createElement('div');
    group.classList.add('input-group');

    if (label && !this.hideLabel) {
      group.appendChild(label);
    } else {
      this.input.classList.add('no-label');
    }

    group.appendChild(this.input);
    this.renderCombined(group, validCallback);

    if (this.help) {
      var help = document.createElement('a');
      help.classList.add('help');
      help.innerText = 'i';
      var helpPopup = document.createElement('div');
      helpPopup.innerText = this.help;
      help.appendChild(helpPopup);
      label === null || label === void 0 ? void 0 : label.appendChild(help);
    }

    if (this.prefix) {
      var prefix = document.createElement('span');
      prefix.classList.add('prefix');
      prefix.innerText = this.prefix;
      this.input.parentElement.insertBefore(prefix, this.input);
    }

    container.appendChild(group);
    return group;
  };

  FormFieldTextarea.fromJson = function (data) {
    var field = new FormFieldTextarea();
    field.type = data.type;
    field.key = data.key;
    field.label = data.label;

    if ('prefix' in data) {
      field.prefix = data.prefix;
    }

    if ('combine' in data) {
      field.combine = data.combine;
    }

    if ('required' in data) {
      field.required = data.required;
    }

    if ('placeholder' in data) {
      field.placeholder = data.placeholder;
    }

    if ('validations' in data) {
      field.validations = data.validations;
    }

    if ('help' in data) {
      field.help = data.help;
    }

    if ('minw' in data) {
      field.minw = data.minw;
    }

    if ('rows' in data) {
      field.rows = data.rows;
    }

    if ('hideLabel' in data) {
      field.hideLabel = data.hideLabel;
    }

    field.loaded();
    return field;
  };

  FormFieldTextarea.prototype.getValueMapping = function () {
    return null;
  };

  return FormFieldTextarea;
}(AbstractFormField_1.default);

exports.default = FormFieldTextarea;
},{"../../../store":"src/store.ts","./AbstractFormField":"src/elements/pageTypes/form/AbstractFormField.ts"}],"src/elements/pageTypes/FormPage.ts":[function(require,module,exports) {
"use strict";

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var FormFieldRadio_1 = __importDefault(require("./form/FormFieldRadio"));

var FormFieldText_1 = __importDefault(require("./form/FormFieldText"));

var FormFieldLabel_1 = __importDefault(require("./form/FormFieldLabel"));

var FormFieldUpload_1 = __importDefault(require("./form/FormFieldUpload"));

var FormFieldCheckbox_1 = __importDefault(require("./form/FormFieldCheckbox"));

var FormFieldSeparator_1 = __importDefault(require("./form/FormFieldSeparator"));

var FormFieldTextarea_1 = __importDefault(require("./form/FormFieldTextarea"));

var FormPage =
/** @class */
function () {
  function FormPage() {
    this.fields = [];
  }

  FormPage.prototype.render = function (container, validCallback) {
    var block = document.createElement('section');
    var hasMandatoryFields = false;
    var validity = new Proxy({}, {
      set: function set(obj, key, value) {
        obj[key] = value;
        var invalidFields = [];

        for (var k in obj) {
          if (!obj[k]) {
            invalidFields.push(k);
          }
        }

        validCallback(invalidFields);
        return true;
      }
    });

    var _loop_1 = function _loop_1(field) {
      if ('undefined' === typeof field.render) {
        console.error('form field has no render method', field);
        throw new Error('form field has no render method');
      }

      if (field.required) {
        hasMandatoryFields = true;
      }

      field.render(block, function (valid) {
        validity[field.key] = valid;
      });
    };

    for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
      var field = _a[_i];

      _loop_1(field);
    }

    container.appendChild(block);
    return hasMandatoryFields;
  };

  FormPage.fromJson = function (c) {
    var page = new FormPage();

    for (var _i = 0, _a = c.fields; _i < _a.length; _i++) {
      var fieldConfig = _a[_i];
      var field = FormPage.createFieldFromJson(fieldConfig); // @ts-ignore

      page.fields.push(field);
    }

    return page;
  };

  FormPage.createFieldFromJson = function (fieldConfig) {
    switch (fieldConfig.type) {
      case 'radio':
        // @ts-ignore
        return FormFieldRadio_1.default.fromJson(fieldConfig);

      case 'text':
        // @ts-ignore
        return FormFieldText_1.default.fromJson(fieldConfig);

      case 'textarea':
        // @ts-ignore
        return FormFieldTextarea_1.default.fromJson(fieldConfig);

      case 'checkbox':
        // @ts-ignore
        return FormFieldCheckbox_1.default.fromJson(fieldConfig);

      case 'label':
        // @ts-ignore
        return FormFieldLabel_1.default.fromJson(fieldConfig);

      case 'upload':
        // @ts-ignore
        return FormFieldUpload_1.default.fromJson(fieldConfig);

      case 'separator':
        // @ts-ignore
        return FormFieldSeparator_1.default.fromJson(fieldConfig);

      default:
        throw new Error('unknown form field type: ' + fieldConfig.type);
    }
  };

  return FormPage;
}();

exports.default = FormPage;
},{"./form/FormFieldRadio":"src/elements/pageTypes/form/FormFieldRadio.ts","./form/FormFieldText":"src/elements/pageTypes/form/FormFieldText.ts","./form/FormFieldLabel":"src/elements/pageTypes/form/FormFieldLabel.ts","./form/FormFieldUpload":"src/elements/pageTypes/form/FormFieldUpload.ts","./form/FormFieldCheckbox":"src/elements/pageTypes/form/FormFieldCheckbox.ts","./form/FormFieldSeparator":"src/elements/pageTypes/form/FormFieldSeparator.ts","./form/FormFieldTextarea":"src/elements/pageTypes/form/FormFieldTextarea.ts"}],"src/elements/Page.ts":[function(require,module,exports) {
"use strict";

var __spreadArray = this && this.__spreadArray || function (to, from) {
  for (var i = 0, il = from.length, j = to.length; i < il; i++, j++) {
    to[j] = from[i];
  }

  return to;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var functions_1 = require("../functions");

var FormPage_1 = __importDefault(require("./pageTypes/FormPage"));

var Page =
/** @class */
function () {
  function Page(name) {
    this.sort = 0;
    this.config = [];
    this.name = name;
    this.slug = functions_1.slugify(name);
  }

  Page.fromJson = function (pageData) {
    for (var _i = 0, _a = ['sort', 'slug', 'name']; _i < _a.length; _i++) {
      var key = _a[_i];

      if ('undefined' === typeof pageData[key]) {
        throw new Error('page is missing "sort" key');
      }
    }

    var page = new Page(pageData.name);
    page.sort = pageData.sort;
    page.slug = pageData.slug;

    for (var _b = 0, _c = pageData.config; _b < _c.length; _b++) {
      var c = _c[_b];

      switch (c.type) {
        case 'form':
          page.config.push(FormPage_1.default.fromJson(c));
          break;

        default:
          throw new Error('unknown page config type: ' + c.type);
      }
    }

    return page;
  };

  Page.prototype.render = function (container, validCallback) {
    container.innerHTML = '';
    var sending = document.createElement('div');
    sending.classList.add('sending-layer');
    sending.innerHTML = '<h1>Senden...</h1>';
    container.appendChild(sending);
    var hasMandatoryFields = false;
    var lastInvalidFields = [];
    var validConfigs = new Proxy({}, {
      set: function set(obj, key, value) {
        obj[key] = value;
        var invalidFields = [];

        for (var config in obj) {
          for (var _i = 0, _a = obj[config]; _i < _a.length; _i++) {
            var key_1 = _a[_i];
            invalidFields.push(key_1);
          }
        }

        if (!lastInvalidFields.length || lastInvalidFields.join('-') !== invalidFields.join('-')) {
          lastInvalidFields = __spreadArray([], invalidFields);
          validCallback(invalidFields.length < 1);
        }

        return true;
      }
    });

    var _loop_1 = function _loop_1(c) {
      // @ts-ignore
      if (c.render(container, function (valid) {
        return validConfigs[c] = valid;
      })) {
        hasMandatoryFields = true;
      }
    };

    for (var _i = 0, _a = this.config; _i < _a.length; _i++) {
      var c = _a[_i];

      _loop_1(c);
    }

    return hasMandatoryFields;
  };

  return Page;
}();

exports.default = Page;
},{"../functions":"src/functions.ts","./pageTypes/FormPage":"src/elements/pageTypes/FormPage.ts"}],"src/elements/Form.ts":[function(require,module,exports) {
"use strict";

var __spreadArray = this && this.__spreadArray || function (to, from) {
  for (var i = 0, il = from.length, j = to.length; i < il; i++, j++) {
    to[j] = from[i];
  }

  return to;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var PageList_1 = __importDefault(require("./PageList"));

var Page_1 = __importDefault(require("./Page"));

var store_1 = __importDefault(require("../store"));

var Form =
/** @class */
function () {
  function Form(name, backendUrl) {
    this.pages = new PageList_1.default();
    this.hasMandatoryFields = false;
    this.currentPage = 1;
    this.name = name;
    this.backendUrl = backendUrl;
    var pageNum = sessionStorage.getItem('pagenum');

    if (pageNum) {
      this.currentPage = parseInt(pageNum);
    }
  }

  Form.fromJson = function (data, backendUrl) {
    if ('undefined' === typeof data.name) {
      throw new Error('json is missing "name" key');
    }

    if ('undefined' === typeof data.pages) {
      throw new Error('json is missing "pages" key');
    }

    var form = new Form(data.name, backendUrl);

    for (var _i = 0, _a = data.pages; _i < _a.length; _i++) {
      var pageData = _a[_i];
      form.addPage(Page_1.default.fromJson(pageData));
    }

    return form;
  };

  Form.prototype.addPage = function (page) {
    this.pages.add(page);
  };

  Form.prototype.render = function (selector) {
    var target = document.querySelector(selector);

    if ('undefined' === typeof target) {
      throw new Error('no element with selector found: ' + selector);
    }

    if (!this.container) {
      this.container = document.createElement('div');
      this.container.classList.add('sf-form');
      target.appendChild(this.container);
    }

    this.update();
  };

  Form.prototype.update = function () {
    this.updateHeader();
    this.updatePage();
    this.updateFooter();
  };

  Form.prototype.renderHeader = function () {
    var container = this.container.querySelector('header');
    var list = null;

    if (!container) {
      container = document.createElement('header');
      list = document.createElement('ul');
      container.appendChild(list);
      this.container.appendChild(container);
    } else {
      list = container.querySelector('ul');
    }

    var items = [];

    for (var _i = 0, _a = this.pages.all(); _i < _a.length; _i++) {
      var page = _a[_i];
      var item = document.createElement('li');
      item.setAttribute('data-page', '' + page.sort);
      var sort = document.createElement('div');
      sort.classList.add('sort');
      sort.innerText = '' + page.sort;
      var name = document.createElement('span');
      name.innerText = page.name;
      item.appendChild(sort);
      item.appendChild(name);
      items.push(item);
    }

    list.innerHTML = '';

    for (var _b = 0, items_1 = items; _b < items_1.length; _b++) {
      var item = items_1[_b];
      list.appendChild(item);
    }

    return container;
  };

  Form.prototype.updatePage = function () {
    var _this = this;

    var container = this.container.querySelector('.page');

    if (!container) {
      container = document.createElement('div');
      container.classList.add('page');
      var headline = document.createElement('h1');
      var pagename = document.createElement('span');
      pagename.classList.add('pagename');
      pagename.innerText = this.pages.get(this.currentPage).name;
      var pagenum = document.createElement('span');
      pagenum.classList.add('pagenum');
      pagenum.innerHTML = " (" + this.currentPage + "/" + this.pages.count() + ")";
      headline.appendChild(pagename);
      headline.appendChild(pagenum);
      this.container.appendChild(headline);
      this.container.appendChild(container);
    } else {
      // @ts-ignore
      this.container.querySelector('h1 .pagename').innerText = this.pages.get(this.currentPage).name; // @ts-ignore

      this.container.querySelector('h1 .pagenum').innerText = " (" + this.currentPage + "/" + this.pages.count() + ")";
    }

    if (this.currentPage === this.pages.count()) {
      this.container.classList.add('last-page');
    } else {
      this.container.classList.remove('last-page');
    }

    var page = this.pages.get(this.currentPage);
    this.isValid = null;

    if (page) {
      // @ts-ignore
      this.hasMandatoryFields = page.render(container, function (valid) {
        if (valid === null || valid !== _this.isValid) {
          _this.isValid = valid;

          if (_this.btnNext) {
            if (valid) {
              _this.btnNext.classList.remove('disabled');

              _this.btnSend.classList.remove('disabled');
            } else {
              _this.btnNext.classList.add('disabled');

              _this.btnSend.classList.add('disabled');
            }
          }
        }
      });
    }
  };

  Form.prototype.updateFooter = function () {
    var _this = this;

    var container = this.container.querySelector('footer');

    if (!container) {
      container = document.createElement('footer');
      this.btnPrev = document.createElement('a');
      this.btnPrev.href = 'javascript:void(0);';
      this.btnPrev.classList.add('btn');
      this.btnPrev.classList.add('btn-prev');
      this.btnPrev.innerHTML = '<span class="arrow">&lt;</span> Zurück';
      this.btnNext = document.createElement('a');
      this.btnNext.href = 'javascript:void(0);';
      this.btnNext.classList.add('btn');
      this.btnNext.classList.add('btn-next');
      this.btnNext.innerHTML = 'Weiter <span class="arrow">&gt;</span>';
      this.btnSend = document.createElement('a');
      this.btnSend.href = 'javascript:void(0);';
      this.btnSend.classList.add('btn');
      this.btnSend.classList.add('btn-send');
      this.btnSend.innerHTML = 'Formular absenden';
      this.btnPrev.addEventListener('click', function () {
        return _this.movePage(-1);
      });
      this.btnNext.addEventListener('click', function () {
        return _this.movePage(+1);
      });
      this.btnSend.addEventListener('click', function () {
        return _this.sendData();
      });
      var buttonContainer = document.createElement('div');
      buttonContainer.classList.add('buttons');
      this.noticeMandatory = document.createElement('div');
      this.noticeMandatory.classList.add('notice-mandatory');
      this.noticeMandatory.innerHTML = '<span>*</span> Pflichtfelder';
      buttonContainer.appendChild(this.btnPrev);
      buttonContainer.appendChild(this.btnNext);
      buttonContainer.appendChild(this.btnSend);
      container.appendChild(buttonContainer);
      container.appendChild(this.noticeMandatory);
      this.container.appendChild(container);
    }

    if (this.currentPage === 1) {
      this.btnPrev.classList.add('disabled');
      this.btnNext.classList.remove('disabled');
      this.btnSend.classList.remove('disabled');
    } else if (this.currentPage === this.pages.count()) {
      this.btnPrev.classList.remove('disabled');
      this.btnNext.classList.add('disabled');
      this.btnSend.classList.add('disabled');
    } else {
      this.btnPrev.classList.remove('disabled');
      this.btnNext.classList.remove('disabled');
      this.btnSend.classList.remove('disabled');
    }

    if (this.isValid === false) {
      this.btnNext.classList.add('disabled');
      this.btnSend.classList.add('disabled');
    } else {
      this.btnNext.classList.remove('disabled');
      this.btnSend.classList.remove('disabled');
    }

    if (this.hasMandatoryFields) {
      this.noticeMandatory.classList.remove('hidden');
    } else {
      this.noticeMandatory.classList.add('hidden');
    }
  };

  Form.prototype.sendData = function () {
    var _this = this;

    var fields = store_1.default.fields,
        data = store_1.default.data;
    var pages = [];

    for (var _i = 0, _a = this.pages.all(); _i < _a.length; _i++) {
      var page = _a[_i];
      var fieldsByConfig = page.config.map(function (c) {
        var res = [];

        for (var _i = 0, _a = c.fields; _i < _a.length; _i++) {
          var field = _a[_i];
          res.push(field.key);

          if ('undefined' !== typeof field.combine && field.combine) {
            for (var _b = 0, _c = field.combine.items; _b < _c.length; _b++) {
              var childField = _c[_b];
              res.push(childField.key);
            }
          }
        }

        return res;
      });
      var fields_1 = [];

      for (var _b = 0, fieldsByConfig_1 = fieldsByConfig; _b < fieldsByConfig_1.length; _b++) {
        var c = fieldsByConfig_1[_b];
        fields_1 = __spreadArray(__spreadArray([], fields_1), c);
      }

      pages.push({
        sort: page.sort,
        name: page.name,
        slug: page.slug,
        fields: fields_1
      });
    }

    var request = {
      name: this.name,
      pages: pages,
      fields: fields,
      data: data
    };
    this.setSending();
    fetch(this.backendUrl + "?submit=1", {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request)
    }).then(function (res) {
      return res.json();
    }).then(function (res) {
      _this.setSending(false);

      if ('undefined' === typeof res.success || !res.success) {
        return alert("Leider ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.");
      }

      var page = _this.container.querySelector('.page');

      page.classList.add('completed');
      page.innerHTML = '<h1>Vielen Dank.</h1><h2>Wir haben Ihre Anfrage erhalten und melden uns schnellstmöglich bei Ihnen.</h2>';

      _this.btnPrev.remove();

      _this.btnSend.remove();

      _this.noticeMandatory.remove();

      _this.container.querySelector('.pagename').remove();

      _this.container.querySelector('.pagenum').remove();

      sessionStorage.removeItem('pagenum');
      sessionStorage.removeItem('sf-form-data');
    }).catch(function () {
      alert("Leider ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.");

      _this.setSending(false);
    });
  };

  Form.prototype.setSending = function (sending) {
    if (sending === void 0) {
      sending = true;
    }

    if (sending) {
      this.container.querySelector('.page').classList.add('sending');
      this.btnPrev.classList.add('disabled');
      this.btnSend.classList.add('disabled');
    } else {
      this.container.querySelector('.page').classList.remove('sending');
      this.btnPrev.classList.remove('disabled');
      this.btnSend.classList.remove('disabled');
    }
  };

  Form.prototype.movePage = function (step) {
    if (this.currentPage + step < 0 || this.currentPage + step > this.pages.count()) return;
    this.currentPage += step;
    sessionStorage.setItem('pagenum', '' + this.currentPage);
    this.update();
  };

  Form.prototype.updateHeader = function () {
    var _this = this;

    var container = this.container.querySelector('header');

    if (!container) {
      container = this.renderHeader();
    }

    container.querySelectorAll('li').forEach(function (item) {
      if (item.hasAttribute('data-page')) {
        var sort = parseInt(item.getAttribute('data-page'));

        if (sort <= _this.currentPage) {
          item.classList.add('active');
        } else {
          item.classList.remove('active');
        }
      }
    });
  };

  return Form;
}();

exports.default = Form;
},{"./PageList":"src/elements/PageList.ts","./Page":"src/elements/Page.ts","../store":"src/store.ts"}],"node_modules/parcel-bundler/src/builtins/bundle-url.js":[function(require,module,exports) {
var bundleURL = null;

function getBundleURLCached() {
  if (!bundleURL) {
    bundleURL = getBundleURL();
  }

  return bundleURL;
}

function getBundleURL() {
  // Attempt to find the URL of the current script and use that as the base URL
  try {
    throw new Error();
  } catch (err) {
    var matches = ('' + err.stack).match(/(https?|file|ftp|chrome-extension|moz-extension):\/\/[^)\n]+/g);

    if (matches) {
      return getBaseURL(matches[0]);
    }
  }

  return '/';
}

function getBaseURL(url) {
  return ('' + url).replace(/^((?:https?|file|ftp|chrome-extension|moz-extension):\/\/.+)?\/[^/]+(?:\?.*)?$/, '$1') + '/';
}

exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
},{}],"node_modules/parcel-bundler/src/builtins/css-loader.js":[function(require,module,exports) {
var bundle = require('./bundle-url');

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    link.remove();
  };

  newLink.href = link.href.split('?')[0] + '?' + Date.now();
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      if (bundle.getBaseURL(links[i].href) === bundle.getBundleURL()) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

module.exports = reloadCSS;
},{"./bundle-url":"node_modules/parcel-bundler/src/builtins/bundle-url.js"}],"scss/sf-form.scss":[function(require,module,exports) {
var reloadCSS = require('_css_loader');

module.hot.dispose(reloadCSS);
module.hot.accept(reloadCSS);
},{"_css_loader":"node_modules/parcel-bundler/src/builtins/css-loader.js"}],"sf-form.ts":[function(require,module,exports) {
"use strict";

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var Form_1 = __importDefault(require("./src/elements/Form"));

require("./scss/sf-form.scss");

window['renderSFForm'] = function (selector, backendUrl) {
  var elem = document.querySelector(selector);

  if (!elem) {
    throw new Error("form div does not exist: " + selector);
  }

  var formId = 0;

  if (elem.hasAttribute('data-form-id')) {
    formId = parseInt(elem.getAttribute('data-form-id'));
  }

  if (!formId) {
    throw new Error('form div does not have required "data-form-id" attribute');
  }

  fetch(backendUrl + "?form-id=" + formId).then(function (res) {
    return res.json();
  }).then(function (config) {
    var form = Form_1.default.fromJson(config, backendUrl);
    form.render(selector);
  });
};
},{"./src/elements/Form":"src/elements/Form.ts","./scss/sf-form.scss":"scss/sf-form.scss"}],"node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "1235" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/parcel-bundler/src/builtins/hmr-runtime.js","sf-form.ts"], null)
//# sourceMappingURL=/sf-form.e8dfc3ea.js.map