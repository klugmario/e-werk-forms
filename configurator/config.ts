import {IPageConfigData} from "./src/elements/Form";

enum Validations {
    'notempty' = 'notempty'
}

export default {
    pages: [
        {
            sort: 1, name: 'Kundendaten', slug: 'kundendaten', config: [
                {
                    type: 'form', fields: [
                        {
                            type: 'radio', key: 'anrede', label: 'Anrede', required: true, options: [
                                {key: 'm', label: 'Herr'},
                                {key: 'f', label: 'Frau'},
                            ]
                        },
                        {type: 'text', key: 'titel', label: 'Titel'},
                        {type: 'text', key: 'vorname', label: 'Vorname', required: true, validations: ['min-len-3']},
                        {type: 'text', key: 'nachname', label: 'Nachname', required: true},
                        {type: 'text', key: 'strasse', label: 'Straße', required: true},
                        {type: 'text', key: 'hausnummer', label: 'Hausnummer', required: true},
                        {
                            type: 'text', key: 'plz', label: 'PLZ', inputType: 'text', required: true, combine: {
                                widths: [15, 85], split: true, items: [
                                    {type: 'text', key: 'ort', label: 'Ort', inputType: 'text', required: true}
                                ]
                            }
                        },
                        {
                            type: 'text',
                            key: 'tel-vorwahl',
                            label: 'Telefon Vorwahl',
                            inputType: 'text',
                            prefix: '+43',
                            minw: 60,
                            required: true,
                            combine: {
                                widths: [10, 80], items: [
                                    {type: 'text', key: 'tel-nummer', label: 'Telefon Nummer', hideLabel: true, inputType: 'text', required: true}
                                ]
                            }
                        },
                        {type: 'text', key: 'email', label: 'E-Mail-Adresse', inputType: 'email', required: true},
                    ]
                }
            ]
        },
        {
            sort: 2, name: 'Gewünschter Tarif', 'slug': 'gewuenschter-tarif', config: [
                {
                    type: 'form', fields: [
                        {
                            type: 'radio', key: 'tarif', label: 'Gewünschter Tarif', mobile: 1000, required: true, options: [
                                {key: 'universaltarif', label: 'Universaltarif', help: 'Nur als Zusatz zum Universaltarif für Haushalt, Landwirtschaft und Gewerbe.'},
                                {key: 'universaltarif-sl', label: 'Universaltarif SL', help: 'Tarif 2'},
                                {key: 'universaltarif-lieferung', label: 'Universaltarif Lieferung', help: 'Tarif 3'},
                            ]
                        }
                    ]
                }
            ]
        },
        {
            sort: 3, name: 'Zahlungsart', slug: 'zahlungsart', config: [
                {
                    type: 'form', fields: [
                        {
                            type: 'radio', key: 'zahlungsart', label: 'Zahlungsart', mobile: 1000, required: true, options: [
                                {key: 'zahlungsanweisung', label: 'Zahlungsanweisung'},
                                {key: 'abbuchungsauftrag', label: 'Abbuchungsauftrag'},
                                {key: 'einzugsermaechtigung', label: 'Einzugsermächtigung'},
                            ]
                        },
                        {
                            type: 'label',
                            key: 'description',
                            label: 'Falls Sie die Zahlungsart Einzugsermächtigung gewählt haben, füllen Sie bitte folgende Felder aus:'
                        },
                        {type: 'text', key: 'bankname', label: 'Bankname'},
                        {type: 'text', key: 'iban', label: 'IBAN'},
                        {type: 'text', key: 'bic', label: 'BIC'}
                    ]
                }
            ]
        },
        {
            sort: 4, name: 'Rechnungszustellung', slug: 'rechnungszustellung', config: [
                {
                    type: 'form', fields: [
                        {
                            type: 'label',
                            label: 'Bitte nur ausfüllen, falls Ihre Rechnungsadresse von Ihren Kundendaten abweicht.'
                        },
                        {type: 'text', key: 'rechnung-vorname', label: 'Vorname'},
                        {type: 'text', key: 'rechnung-nachname', label: 'Nachname'},
                        {type: 'text', key: 'rechnung-strasse', label: 'Straße'},
                        {type: 'text', key: 'rechnung-hausnummer', label: 'Hausnummer'},
                        {
                            type: 'text', key: 'rechnung-plz', label: 'PLZ', inputType: 'text', combine: {
                                widths: [15, 85], split: true, items: [
                                    {type: 'text', key: 'rechnung-ort', label: 'Ort', inputType: 'text'}
                                ]
                            }
                        },
                    ]
                }
            ]
        },
        {
            sort: 5, name: 'Identifikation', slug: 'identifikation', config: [
                {
                    type: 'form', fields: [
                        {
                            type: 'radio',
                            key: 'identifikation',
                            label: 'Art der Identifikation',
                            mobile: 700,
                            required: true,
                            options: [
                                {key: 'fuehrerschein', label: 'Führerschein'},
                                {key: 'reisepass', label: 'Reisepass'},
                                {key: 'personalausweis', label: 'Personalausweis'},
                            ]
                        },
                        {type: 'text', key: 'identifikationsnummer', label: 'Identifikationsnummer', help: 'Reisepassnummer, Führerscheinnummer', required: true},
                        {type: 'text', key: 'behoerde', label: 'Ausstellende Behörde', required: true},
                        {type: 'upload', key: 'identifikationskopie', label: 'Kopie der Identifikation', required: true},
                        {type: 'separator'},
                        {type: 'checkbox', key: 'zustimmung', label: 'Ja, ich stimme den folgenden Bedingungen zu', required: true},
                        {type: 'label', align: 'left', label: '' +
                                '<p class="bodytext">Ja, ich habe die <a href="/fileadmin/user_upload/PDF/allg_stromlieferbedingungen_ab_1.3.2020.pdf" title="Leitet Herunterladen der Datei ein" class="download">Allgemeinen Bedingungen für die Lieferung elektrischer Energie</a> für Kunden der Elektrizitätswerk Fernitz Ing. Purkarthofer GmbH &amp; Co KG und das Informationsblatt für Universaltarif gelesen und bin mit deren Inhalt einverstanden.\n' +
                                '</p>\n' +
                                '<p class="bodytext">Ich/Wir ermächtige/n und bevollmächtige/n die Elektrizitätswerke Fernitz Ing. Purkarthofer GmbH &amp; Co KG (nachstehend kurz "E-Werk Fernitz" genannt) mich/uns gegenüber Dritten in allen Angelegenheiten zu vertreten, die notwendig sind, um die Belieferung meiner/unserer Kundenanlage/n mit Strom und die gemeinsamen Entgelte (Netz, Energie) durch das E-Werk Fernitz zu ermöglichen. Diese Vollmacht umfasst insbesondere das Recht zur Kündigung der bestehenden Energielieferverträge bei dem bisherigen Energielieferanten sowie zum Widerspruch gegen eine Preisänderung im Wege einer Änderungskündigung. Ich bin/Wir sind einverstanden, dass das Entgelt für die Energielieferung gemeinsam mit dem Netznutzungsentgelt verrechnet wird und dass eine erteilte Bankeinzugsermächtigung auch dafür herangezogen werden kann.\n' +
                                '</p>\n' +
                                '<p class="bodytext">Ich/Wir stimme/n zu, dass das E-Werk Fernitz meine/unsere Daten zur Erledigung meiner/unserer Anliegen in Zusammenhang mit einer Stromlieferung sowie für die postalische und elektronische Zusendung von Informationen (z.B. Werbekataloge, Produktinformationen) verarbeitet. Diese Zustimmenserklärung kann ich jederzeit schriftlich durch ein Mail an <a href="mailto:office@ewerkfernitz.at" title="Öffnet ein Fenster zum Versenden der E-Mail">office@ewerkfernitz.at</a> widerrufen.\n' +
                                '</p>\n' +
                                '<p class="bodytext">Ja, ich habe das <a href="/fileadmin/user_upload/PDF/Widerrufsrecht_Online_Vertragsabschluss.pdf" title="Leitet Herunterladen der Datei ein">Widerrufsrecht für Onlinewechsel</a> für Kunden der Elektrizitätswerk Fernitz Ing. Purkarthofer GmbH &amp; Co KG gelesen und bin mit dem Inhalt einverstanden.\n' +
                                '</p>\n' +
                                '<p class="bodytext">Ich wünsche, dass der Beginn der Belieferung meiner Anlage mit elektrischer Energie so rasch wie möglich stattfinden soll, jedenfalls vor Ende des mir zustehenden 14 tägigen Rücktrittsrechtes gem. § 3 Z 1 FAGG (Vertrag wurde außerhalb von Geschäftsräumen geschlossen) oder gem. § 11 FAGG (Fernabsatzvertrag: Vertragsabschluss per E-Mail oder Internet) oder gem. § 3 KSchG (Vertragserklärung wurde weder in den vom Unternehmen für seine geschäftlichen Zwecke dauernd benutzten Räumen noch bei einem vom Unternehmen auf einer Messe oder einem Markt benutzten Stand abgegeben) und erkläre mich – im Falle meines vorzeitigen Rücktritts - ausdrücklich damit einverstanden, jenen Betrag zu zahlen, der dem Anteil der bis zum Rücktritts-Zeitpunkt bereits erbrachten Dienstleistungen oder Lieferungen von Strom im Vergleich zum Gesamtumfang der im Vertrag vorgesehenen Dienstleistungen oder Lieferungen von Strom entspricht.\n' +
                                '</p>\n' +
                                '<p class="bodytext">Gemäß&nbsp; Datenschutz-Grundverordnung&nbsp; (DSGVO)&nbsp; sind&nbsp; wir dazu verpflichtet, Sie darüber zu informieren, warum wir Ihre Daten erheben und in welcher Form sie verarbeitet werden. Alle Informationen dazu finden Sie unter <a href="/fileadmin/user_upload/PDF/Information_zur_Datenverarbeitung_gemaess_DSGVO.pdf">Information_zur_Datenverarbeitung_gemaess_DSGVO.pdf</a>.</p></div>'},
                    ]
                }
            ]
        }
    ]
} as IPageConfigData
