<?php
/**
 * Plugin Name:       E-Werk Formulare
 * Description:       Formular-Konfigurator und Anzeige-Block
 * Version:           1.0.0
 * Author:            Mario Klug <mario.klug@sourcefactory.at>
 */

use PHPMailer\PHPMailer\PHPMailer;

error_reporting(E_ALL);
ini_set('display_errors', true);

$baseDir = dirname($_SERVER['SCRIPT_FILENAME']);
for ($i = 0; $i < 3; $i++) {
    $baseDir = dirname($baseDir);
}

if ( ! function_exists('get_option')) {
    if (file_exists($baseDir . DIRECTORY_SEPARATOR . 'wp-load.php')) {
        require_once $baseDir . DIRECTORY_SEPARATOR . 'wp-load.php';
    } else {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

        $env = [];
        foreach (['RCPT', 'HOST', 'PORT', 'SECURE', 'USERNAME', 'PASSWORD'] as $key) {
            $env[strtolower($key)] = getenv("MAIL_$key");
        }

        function get_option(string $key)
        {
            global $env;

            switch ($key) {
                case 'ewerk_theme_mailform-rcpt':
                    return $env['rcpt'];
            }

            return null;
        }

        function add_action(string $key, callable $callback)
        {
            return null;
        }

        function wp_mail(string $rcpt, string $subject, string $body, array $headers = [], array $attachments = []): bool
        {
            global $env;

            $mailer = new PHPMailer();
            $mailer->isHTML();
            $mailer->CharSet = PHPMailer::CHARSET_UTF8;

            $mailer->addAddress($rcpt);

            $mailer->Subject = $subject;
            $mailer->Body = $body;

            foreach ($attachments as $path) {
                $mailer->addAttachment($path);
            }

            $mailer->isSMTP();
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = $env['secure'];
            $mailer->Port = $env['port'];
            $mailer->Host = $env['host'];
            $mailer->Username = $env['username'];
            $mailer->Password = $env['password'];

            if ( ! $mailer->send()) {
                echo json_encode(['error' => 'failed to send email', 'info' => $mailer->ErrorInfo]);
                exit(1);
            }

            return true;
        }
    }
}

function createPdfFromHtml(string $html, string $filename)
{
    $pdfFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $filename;
    $htmlFile = $pdfFile . '.html';

    file_put_contents($htmlFile, utf8_decode($html));

    exec("cat $htmlFile | wkhtmltopdf - $pdfFile 2>&1", $output);
    unlink($htmlFile);

    return $pdfFile;
}

$rcpt = get_option('ewerk_theme_mailform-rcpt');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');

if ('OPTIONS' === $_SERVER['REQUEST_METHOD']) {
    exit(0);
}

if (isset($_GET['get-forms'])) {
    header('Content-Type: application/json');

    $files = glob(__DIR__ . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . '*.json');

    $forms = [];
    foreach ($files as $file) {
        if (preg_match('/^(\d+)-(.+)\.json$/', basename($file), $matches)) {
            $data = json_decode(file_get_contents($file));
            $forms[] = [
                'id' => $matches[1],
                'name' => $data->name
            ];
        }
    }

    echo json_encode($forms, JSON_PRETTY_PRINT);
    exit(0);
}

if (isset($_GET['form-id'])) {
    header('Content-Type: application/json');

    $files = glob(__DIR__ . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . $_GET['form-id'] . '-*');

    if ( ! $files || count($files) !== 1) {
        echo json_encode(['error' => 'NOT_FOUND']);
        exit(1);
    }

    require_once $files[0];
    exit(0);
}

if (isset($_GET['submit']) && 'POST' === $_SERVER['REQUEST_METHOD']) {
    header('Content-Type: application/json');

    $json = file_get_contents('php://input');
    $request = json_decode($json);

    if ( ! $request) {
        echo json_encode(['error' => 'no valid request provided']);
        exit(1);
    }

    foreach (['name', 'pages', 'fields', 'data'] as $key) {
        if ( ! isset($request->$key)) {
            echo json_encode(['error' => 'missing request data: ' . $key]);
            exit(1);
        }
    }

    $files = [];
    $tempDirPath = tempnam(sys_get_temp_dir(), 'form_attachments_');
    if (file_exists($tempDirPath)) unlink($tempDirPath);

    $formName = $request->name;

    $dataByPages = [];
    foreach ($request->pages as $page) {
        $pageData = [
            'name' => $page->name,
            'data' => []
        ];

        foreach ($page->fields as $key) {
            if ( ! isset($request->fields->$key) || ! isset($request->data->$key)) {
                continue;
            }

            $field = $request->fields->$key;
            $fieldName = $field->label;
            $value = $request->data->$key;

            if (isset($field->mapping) && isset($field->mapping->$value)) {
                $value = $field->mapping->$value;
            }

            // file
            if (is_object($value) && isset($value->content)) {
                if ( ! file_exists($tempDirPath)) {
                    mkdir($tempDirPath, 0755);
                }

                $path = $tempDirPath . DIRECTORY_SEPARATOR . $value->name;

                list($_, $b64) = explode(',', $value->content);
                file_put_contents($path, base64_decode(str_replace(' ', '+', $b64)));

                $files[] = $path;

                $value = 'Anhang: ' . $value->name;
            }

            if ($fieldName && $value) {
                $pageData['data'][] = ['key' => $fieldName, 'value' => $value];
            }
        }

        if (count($pageData['data'])) {
            $dataByPages[] = $pageData;
        }
    }

    if ( ! $dataByPages) {
        echo json_encode(['error' => 'failed to build mail body']);
        exit(1);
    }

    $html = <<<EOF
<html>
<style>
body { background: #f56020; }
section { width: 500px; background: #fff; margin: 20px auto; padding: 30px; }
h4 { font-size: 20px; color: #f56020; margin: 0 0 20px; }
</style>
<body>
EOF;
    foreach ($dataByPages as $page) {
        $html .= '<section>';
        $html .= "<h4>{$page['name']}</h4>";

        foreach ($page['data'] as $field) {
            $html .= "<strong>{$field['key']}:</strong><br>";
            $html .= nl2br($field['value']) . "<br><br>";
        }

        $html .= '</section>';
    }

    $html .= '</body></html>';

//    $pdfPath = createPdfFromHtml($html, 'Formular.pdf');
//    $files[] = $pdfPath;

    $success = wp_mail($rcpt, $formName, $html, ['Content-Type: text/html; charset=UTF-8'], $files);

//    unlink($pdfPath);

    if ($tempDirPath && file_exists($tempDirPath)) {
        foreach (glob($tempDirPath . DIRECTORY_SEPARATOR . '/*') as $file) {
            if (in_array(basename($file), ['.', '..'])) {
                continue;
            }
            unlink($file);
        }

        rmdir($tempDirPath);
    }

    if ( ! $success) {
        echo json_encode(['error' => 'failed to send email']);
        exit(1);
    }

    echo json_encode(['success' => true]);
    exit(0);
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'block' . DIRECTORY_SEPARATOR . 'forms-block.php';

add_action( 'init', function() {
    wp_enqueue_script('sf-form', plugin_dir_url(__FILE__) . '/configurator/dist/sf-form.js');
    wp_enqueue_style('sf-form', plugin_dir_url(__FILE__) . '/configurator/dist/sf-form.css');
});

if ( ! $rcpt) {
    add_action( 'admin_notices', function () {
        ?>
        <div class="notice notice-info">
            <p>
                Es wurde noch kein E-Mail Empfänger für Formulare angegeben!<br>
                Bitte legen Sie unter <a href="<?php bloginfo('url') ?>/wp-admin/admin.php?page=theme-settings">Theme-Einstellungen</a> eine Adresse fest.
            </p>
        </div>
        <?php
    });
}
